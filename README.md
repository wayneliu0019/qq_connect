# QQ connect plugin for Discourse / Discourse QQ 互联插件

Authenticate with discourse with qq connect.

通过 QQ 互联登陆 Discourse。

## Register Client Key & Secert / 申请 QQ 接入

1. 登录 [QQ Connect](http://connect.qq.com/)，注册填写相关信息。
2. 进入`管理中心`，点击`创建应用`，选择`网站`。
3. 填写相关信息。`网站地址`应填写论坛所处的位置。`回调地址`应填写根域名位置。如图所示。（验证所需要的标签可在 Discourse 设置中插入，验证后即可删除；访问 Discourse 管理面板 - 内容 - 页面顶部）
4. 找到刚申请到的应用，在左上角找到`id`和`key`，分别填入 Discourse 设置中的 `client_key` 和 `client_secret`。

<img src="https://meta.discourse.org/uploads/default/34523/414f622b202bba06.png" width="583" height="500"> 

## Installation / 安装

Run `rake plugin:install repo=https://github.com/fantasticfears/qq_connect` in your discourse directory

In development mode, run `rake assets:clean`

In production, recompile your assets: `rake assets:precompile`

在您 Discourse 目录中运行 `rake plugin:install repo=https://github.com/fantasticfears/qq_connect`

在开发者模式下，运行 `rake assets:clean`

在生产模式下，重编译 assets `rake assets:precompile`

## Usage / 使用

Go to Site Settings's login category, fill in the client id and client secret.

进入站点设置的登录分类，填写 client id 和 client serect。

## Issues / 问题

Visit [topic on Discourse Meta](https://meta.discourse.org/t/qq-login-plugin-qq/19718) or [GitHub Issues](https://github.com/fantasticfears/qq_connect/issues).

访问[Discourse Meta 上的主题](https://meta.discourse.org/t/qq-login-plugin-qq/19718)或[GitHub Issues](https://github.com/fantasticfears/qq_connect/issues)。

## Changelog

Current version: 0.1.1

<img src="https://meta.discourse.org/uploads/default/34493/dc792b8ba0ca145a.png" width="690" height="386">

<img src="https://meta.discourse.org/uploads/default/34492/62b8bfde277857af.png" width="690" height="312">

<img src="https://meta.discourse.org/uploads/default/34494/ea6c21600bd68279.png" width="690" height="330">

<img src="https://meta.discourse.org/uploads/default/34495/eaf2d4fae5f6a64c.png" width="690" height="313">



## 手动操作测试

https://wiki.connect.qq.com/%e5%bc%80%e5%8f%91%e6%94%bb%e7%95%a5_client-side

connect.qq.com中成功注册了application，callback地址为 https://forum.gitlab.cn/auth/qq/callback

### 获取Access Token
https://graph.qq.com/oauth2.0/authorize?response_type=token&client_id=[YOUR_APPID]&redirect_uri=[YOUR_REDIRECT_URI]

YOUR_APPID: application创建好后，返回的app id
YOUR_REDIRECT_URI： 就是https://forum.gitlab.cn/auth/qq/callback

会跳转到qq授权登录页面

如果用户点击“授权并登录”，则成功跳转到指定的redirect_uri，并在URL后加“#”号，带上Access Token以及expires_in等参数。如果redirect_uri地址后已经有“#”号，则加“&”号，带上相应的返回参数。
本例子中就是：
https://forum.gitlab.cn/auth/qq/callback?#access_token=[access token]&expires_in=7776000



### 使用Access Token来获取用户的OpenID
https://graph.qq.com/oauth2.0/me?access_token=6C10CE5B4D3345F70079720211CA89C6
{
client_id: "101947862",
openid: "CB3FE0CA3BDB2560B2CA122470B27E7A"
}


### 使用Access Token以及OpenID来访问和修改用户数据
https://graph.qq.com/user/get_user_info?access_token=6C10CE5B4D3345F70079720211CA89C6&oauth_consumer_key=101947862&openid=CB3FE0CA3BDB2560B2CA122470B27E7A
{
ret: 0,
msg: "",
is_lost: 0,
nickname: "刘巍锋",
gender: "男",
gender_type: 1,
province: "河南",
city: "南阳",
year: "1984",
constellation: "",
figureurl: "http://qzapp.qlogo.cn/qzapp/101947862/CB3FE0CA3BDB2560B2CA122470B27E7A/30",
figureurl_1: "http://qzapp.qlogo.cn/qzapp/101947862/CB3FE0CA3BDB2560B2CA122470B27E7A/50",
figureurl_2: "http://qzapp.qlogo.cn/qzapp/101947862/CB3FE0CA3BDB2560B2CA122470B27E7A/100",
figureurl_qq_1: "http://thirdqq.qlogo.cn/g?b=oidb&k=icnts7sDfjKia0bdzQFT8iahA&s=40&t=1555308967",
figureurl_qq_2: "http://thirdqq.qlogo.cn/g?b=oidb&k=icnts7sDfjKia0bdzQFT8iahA&s=100&t=1555308967",
figureurl_qq: "http://thirdqq.qlogo.cn/g?b=oidb&k=icnts7sDfjKia0bdzQFT8iahA&s=640&t=1555308967",
figureurl_type: "1",
is_yellow_vip: "0",
vip: "0",
yellow_vip_level: "0",
level: "0",
is_yellow_year_vip: "0"
}

只能获取到指定用的昵称，图标以及性别信息，其他信息获取不到。没有诸如qq号，密码等信息
https://wiki.connect.qq.com/%e5%bc%80%e5%8f%91%e8%81%94%e8%b0%83%e7%9b%b8%e5%85%b3%e9%97%ae%e9%a2%98

